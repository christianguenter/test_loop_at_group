REPORT zguc_test_loop_at_group.

CLASS application DEFINITION.

  PUBLIC SECTION.
    METHODS: run.

  PRIVATE SECTION.
    METHODS:
      get_current_message2
        IMPORTING
          it_return        TYPE bapiret2_tab
        RETURNING
          VALUE(rs_return) TYPE bapiret2,

      get_current_message
        IMPORTING
          it_return        TYPE bapiret2_tab
        RETURNING
          VALUE(rs_return) TYPE bapiret2,
      priority
        IMPORTING
          i_type            TYPE bapiret2-type
        RETURNING
          VALUE(r_priority) TYPE i.

ENDCLASS.

CLASS application IMPLEMENTATION.

  METHOD run.

    DATA(lt_return) = VALUE bapiret2_tab( ( type = 'I' )
                                          ( type = 'E' )
                                          ( type = 'E' )
                                          ( type = 'E' )
                                          ( type = 'S' )
                                          ( type = 'E' )
                                          ( type = 'X' )
                                          ( type = 'E' ) ).

    cl_demo_output=>display(  ).


    DATA(ls_return) = get_current_message2( lt_return ).
    cl_demo_output=>write( ls_return ).

    ls_return = get_current_message2( lt_return ).
    cl_demo_output=>write( ls_return ).

    cl_demo_output=>display(  ).

  ENDMETHOD.


  METHOD get_current_message.

    LOOP AT it_return ASSIGNING FIELD-SYMBOL(<ls_return>)
                      GROUP BY priority( <ls_return>-type ).
      rs_return = <ls_return>.
      RETURN.
    ENDLOOP.

  ENDMETHOD.


  METHOD get_current_message2.

    LOOP AT it_return ASSIGNING FIELD-SYMBOL(<ls_return>)
                      GROUP BY translate( val  = <ls_return>-type
                                          from = `AXEWSI `
                                          to   = `0123456` ).
      rs_return = <ls_return>.
      RETURN.
    ENDLOOP.

  ENDMETHOD.


  METHOD priority.

    r_priority = SWITCH #( i_type WHEN 'A' THEN 0
                                  WHEN 'X' THEN 1
                                  WHEN 'E' THEN 2
                                  WHEN 'W' THEN 3
                                  WHEN 'S' THEN 4
                                  WHEN 'I' THEN 5
                                  ELSE 6 ).

  ENDMETHOD.

ENDCLASS.

START-OF-SELECTION.
  NEW application( )->run( ).
